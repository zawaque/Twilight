# Guide en français 🇫🇷
#### Twilight | Sponge 1.12.2 🧽
Ce projet d'avait pas de nom, on l'a ouvert plusieurs fois pour y jouer.
<br/>C'était un serveur avait **des mods comme Galacticraft et Twilight Forest**.
<br/>Il a permis de tuer des heures de confinement à certains 😉.

#### Installation du serveur 🛠️
Pour l'instant, vous pouvez juste mettre les fichiers dans votre serveur via FTP.
<br/>Un script est prévu pour installer le serveur de A à Z pour les VPS.
<br/>Ou vous pouvez juste <a href="https://drive.google.com/drive/folders/1sj5E27TkJCfMeaI48T2eNtnXwTMQIhCE?usp=sharing">télécharger les mods en cliquant juste ici</a> 👈.

#### En conteneur Docker 🐳 ?
Alors loin de moi à vous expliquer comment fonctionne Docker, *(vu que j'apprends moi-même comment il marche)* mais vous pourrez installer le serveur avec Docker.
<br/>*Un script sera prévu pour, pas de panique, laissez moi du temps* 🥲.
<li><a href="https://hub.docker.com/repository/docker/leohogwart/twilight">Pour installer le conteneur</a></li>
<li><a href="https://www.youtube.com/watch?v=SXB6KJ4u5vg&list=PL8SZiccjllt1jz9DsD4MPYbbiGOR_FYHu">Pour en savoir plus sur Docker</a></li>

#### Support 🙋
Vous avez un soucis avec votre serveur ? Une idée ?
Vous pouvez me contacter via <a href="https://twitter.com/LeoHogwart">Twitter</a>
<br/>Ou en venant sur le serveur qui a vu naître ce "projet" : https://discord.gg/cGR7XnSXym

### <br/>*sorry, I only speak "oui oui baguette, croissant au fromage" langage, a translation will arrive soon* 🥲

<img src="https://github.com/LeoHogwart/twilight/blob/main/screen/2021-01-23_18.28.17.png?raw=true" width="980"/> 
<img src="https://github.com/LeoHogwart/twilight/blob/main/screen/2021-01-24_09.27.09.png?raw=true" width="980"/>
<img src="https://github.com/LeoHogwart/twilight/blob/main/screen/2021-01-25_19.26.39.png?raw=true" width="980"/> 
<img src="https://github.com/LeoHogwart/twilight/blob/main/screen/2021-01-27_15.02.19.png?raw=true" width="980"/>
<img src="https://github.com/LeoHogwart/twilight/blob/main/screen/2021-01-30_18.42.06.png?raw=true" width="980"/>
